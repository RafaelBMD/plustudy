<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});

// Route::middleware(['cors'])->group(
// Route::resources(
//   [
//     'subjects' => 'SubjectController',
//     'contents' => 'ContentController'
//   ]
// );
// );
Route::middleware(['cors', 'api'])->group(
  function () {
    Route::resources(
      [
        'subjects' => 'SubjectController',
        'contents' => 'ContentController',
        'horaries' => 'HoraryController',
        'groups' => 'GroupController',
        'group_chat' => 'GroupChatController',
        'content_group' => 'ContentGroupController',
        'content_horary' => 'ContentHoraryController',
        'content_subject' => 'ContentSubjectController',
        'horary_user' => 'HoraryUserController',
        'user' => 'UserController',
      ]
    );
//    Route::get('horaries_user', 'HoraryController@horariesUser');
    Route::get('horaries_user', 'HoraryController@horariesUser');
//    Route::get('/update_all', function () {
//      return 123;
//    });
    Route::post('/update_all', 'GeneralController@updateAll');
  }
);

Route::group(
  [
    'prefix' => 'auth',
    'middleware' => ['cors', 'api'],
  ],
  function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'AuthController@register');

    Route::post('/forgot-password', 'AuthController@forgotPassword');
    Route::post('/reset-password', 'AuthController@resetPassword');

    Route::post('/facebook', 'AuthController@facebook');
    Route::post('/google', 'AuthController@google');

    Route::get('/check_user', 'AuthController@checkUser');
    Route::get('/refresh', 'AuthController@refresh');
  }
);

Route::group(
  [
    'prefix' => 'auth',
    'middleware' => ['cors', 'api'],
  ],
  function () {
    Route::post('/logout', 'AuthController@logout');
    Route::post('/impersonate', 'AuthController@impersonate');
    Route::get('/user', 'AuthController@user');
  }
);

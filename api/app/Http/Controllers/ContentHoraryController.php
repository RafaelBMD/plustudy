<?php

namespace App\Http\Controllers;

use App\Models\ContentHorary;

class ContentHoraryController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (ContentHorary $model) {
    $this->model = $model;
    $this->with  = [];
  }
}

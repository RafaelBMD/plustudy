<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Horary;
use App\Models\HoraryUser;
use App\Models\Subject;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class GeneralController extends Controller
{
  protected $model;
  protected $with;

//  public function __construct () {}

  public function updateAll(Request $request)
  {
    $arr = [];
//    return $request->updateItems;
    $updateItems = json_decode($request->updateItems);
    foreach ($updateItems as $key => $value) {

      $content = false;
      switch ($value->table) {
        case 'horaries':
//          $fk = 'horary_id';
//            return $value->subject_id;
          $content = true;
          $this->model = new Horary();
          break;
        case 'subjects':
//          $fk = 'subject_id';
          $content = true;
          $this->model = new Subject();
          break;
        case 'groups':
//          $fk = 'group_id';
          $content = true;
          $this->model = new Group();
          break;
      }


      // Verifica se existe a fk subject_id e se o valor é maior que 1 milhao(sendo valores criados offline) se existir altera o seu valor para o id criado no BD
      if (array_key_exists('subject_id', $value) and (int)($value->subject_id) > 1000000) {
        $value->subject_id = $arr[$value->subject_id];
      }
      if (array_key_exists('group_id', $value) and (int)($value->group_id) > 1000000) {
        $value->group_id = $arr[$value->group_id];
      }


//      $this->model = new Subject();
//      $result = DB::table('subjects')->insert((array)$value);
      switch ($value->type) {
        case 'create':
          $result = $this->model->create((array)$value);
          $arr[$value->id] = $result->id;
          switch ($value->table) {
            case 'horaries':
              HoraryUser::create(['user_id' => JWTAuth::parseToken()->authenticate()->id, 'horary_id' => $result->id]);
              break;
          }
          // Verifica se existe conteudos no registro se tiver salva os conteudos
          if (array_key_exists('contents', $value)) {
            //transforma os conteudos em array para salvar no banco
            $result->contents()->createMany(json_decode(json_encode($value->contents), true));
          }
//          if ($fk !== '') {
//            $updateItems = json_decode(str_replace($value->id, $result->id, json_encode($updateItems)));
//            return $updateItems;
//            foreach ($updateItems as $k => $val) {
//              $teste = (array)$val;
//              if (array_key_exists($fk,$teste) and $teste[$fk] === $value->id) {
//                $val.$fk = $result->id;
//              }
//            }
//          }
          break;
        case 'update':
//          $this->model->findOrFail($id)->update($request->all());
          $result = $this->model->find($value->id);
          $result->update((array)$value);
          if (array_key_exists('contents', $value)) {
            //Busca deleta e depois cria todos os conteudos
            $result->contents()->delete();
            $result->contents()->createMany(json_decode(json_encode($value->contents), true));
          }
          break;
        case 'delete':
          $result = $this->model->where('id', '=', $value->id)->delete();
          break;
      }
    }
  }
}

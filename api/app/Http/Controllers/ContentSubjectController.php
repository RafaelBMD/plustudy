<?php

namespace App\Http\Controllers;

use App\Models\ContentSubject;

class ContentSubjectController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (ContentSubject $model) {
    $this->model = $model;
    $this->with  = [];
  }
}

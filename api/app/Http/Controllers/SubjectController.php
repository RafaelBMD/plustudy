<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (Subject $model) {
    $this->model = $model;
    $this->with  = ['contents'];
  }

  public function afterStore ($result, $request) {
    if ($request->contents) {
      $result->contents()->createMany($request->contents);
    }
  }

  public function afterUpdate ($oldValue, $request, $id) {
    if ($request->contents) {
      $subject = Subject::find($id);
      $subject->contents()->delete();
      $subject->contents()->createMany($request->contents);
    }
  }
}

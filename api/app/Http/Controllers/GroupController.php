<?php

namespace App\Http\Controllers;

use App\Models\Group;

class GroupController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (Group $model) {
    $this->model = $model->isParticipant();
    $this->with  = ['contents', 'horaries', 'users'];
  }

  public function afterStore ($result, $request) {
    if ($request->contents) {
      $result->contents()->createMany($request->contents);
    }
    if ($request->users_id) {
      $result->users()->attach($request->users_id);
    }
  }

  public function afterUpdate ($oldValue, $request, $id) {
    $group = Group::find($id);
    if ($request->contents) {
      $group->contents()->delete();
      $group->contents()->createMany($request->contents);
    }
    $group->users()->sync($request->users_id);
  }
}

<?php
namespace App\Http\Controllers;

use App\Mail\ForgotPassword;
use App\Models\Access;
use App\Models\AccessUser;
use App\Models\Anobase;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\TaskStatus;
use App\PasswordReset;
use App\Scopes\YearGlobalScope;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Socialite;
use JWTAuth;

class AuthController extends Controller {

  /**
   * Get a JWT via given credentials.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function login () {
    $credentials = request(['email', 'password']);

    if (!$token = auth()->attempt($credentials)) {
      return response()->json(['error' => 'Unauthorized'], 401);
    }

    return $this->respondWithToken($token);
    // return response([
    //   'status' => 'success',
    // ])
    //   ->header('Authorization', $token);
  }

  /**
   * Get the authenticated User.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function user () {
    return response(
      [
        'status' => 'success',
        'data'   => User::find(auth()->user()->id),
      ]
    );
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh () {
//    return $this->respondWithToken(auth()->refresh());
  }

  /**
   * Log the user out (Invalidate the token).
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout () {
    auth()->logout();

    return response()->json(['message' => 'Successfully logged out']);
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken ($token) {
    return response()->json([
      'access_token' => $token,
      'user' => User::find(auth()->user()->id),
      'token_type'   => 'bearer',
      'expires_in'   => auth()->factory()->getTTL() * 60,
    ])->header('Authorization', $token);
  }

  public function register (Request $request) {
    $request->merge([
      'password' => \Hash::make($request->get('password')),
    ]);
    $user = new User();
    $user = $user->create(request()->all());

    $token = auth()->login($user);

    return response(
      [
        'status' => 'success',
        'user'   => $user,
        'token'  => $token,
      ],
      200
    );
  }

  public function forgotPassword (Request $request) {
    $user = User::where('email', $request->input('email'))->first();
    if (!$user) {
      return response(['data' => 'Check if the email is correct'], 403);
    }
    $token = PasswordReset::create(
      [
        'user_id'   => $user->id,
        'token'     => uniqid(),
        'expire_at' => Carbon::now()->addHour(),
      ]
    );
    Mail::to($user)->send(new ForgotPassword($token, $request));

    return response(['data' => 'Email sent.'], 200);
  }

  /**
   * Hanlding the request to reset the password
   */
  public function resetPassword (Request $request) {
    $validator = Validator::make(
      $request->all(),
      [
        'password'     => 'required|min:6',
        'confirmation' => 'required|same:password',
      ]
    );
    if ($validator->fails()) {
      return response(['data' => $validator->errors()], 433);
    }
    $token   = $request->input('token');
    $dBToken = DB::table('password_resets')
      ->where('token', $token)
      ->where('expire_at', '>', Carbon::now())
      ->first();
    if (!$dBToken) {
      return response(['data' => 'Wrong token.'], 403);
    }
    $user           = User::where('id', $dBToken->user_id)->first();
    $user->password = bcrypt($request->input('password'));
    $user->save();
    DB::table('password_resets')->where('id', $dBToken->id)->delete();

    return response(['data' => 'Password changed.'], 200);
  }

  public function checkUser (Request $request) {
    return response(User::where('email', $request->email)->get());
  }

  public function redirectToProvider ($provider) {
    return Socialite::driver($provider)->redirect();
  }

  public function handleProviderCallback ($provider) {
    $socialUser = Socialite::driver($provider)->stateless()->user();
    $user       = User::where('email', $socialUser->email)->first();
    if ($user) {
      $user->pass_token = $socialUser->token;
      $user->save();
    } else {
      User::insert([
        'email'      => $socialUser->email,
        'id_text'    => $socialUser->name,
        'pass_token' => $socialUser->token,
        'company_id' => 1, //Modificar para buscar correto
      ]);
    }

    return redirect('http://localhost:8086/#/login?token='.$socialUser->token.'&provider='.$provider);
  }

  public function facebook (Request $request) {
    return $this->social($request, 'facebook');
  }

  public function google (Request $request) {
    return $this->social($request, 'google');
  }

  private function social (Request $request) {
    if ($request->has('token')) {
      $user = User::where('pass_token', $request->token)->first();

      if (!$token = JWTAuth::fromUser($user)) {
        throw new AuthorizationException;
      }

      return $this->respondWithToken($token);

      // return response([
      //   'status' => 'success',
      //   'msg'    => 'Successfully logged in via '.$provider.'.',
      // ])
      //   ->header('Authorization', $token);
    }
  }
}


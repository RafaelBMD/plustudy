<?php

namespace App\Http\Controllers;

use App\Models\GroupChat;

class GroupChatController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (GroupChat $model) {
    $this->model = $model;
    $this->with  = ['group', 'user'];
  }
}

<?php

namespace App\Http\Controllers;

use App\Models\ContentGroup;

class ContentGroupController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (ContentGroup $model) {
    $this->model = $model;
    $this->with  = [];
  }
}

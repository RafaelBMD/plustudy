<?php

namespace App\Http\Controllers;

use App\Models\Horary;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class HoraryController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (Horary $model) {
    $this->model = $model;
    $this->with  = ['contents', 'subject', 'group'];
  }

  public function afterStore ($result, $request) {
    if ($request->contents) {
      $result->contents()->createMany($request->contents);
    }
    $result->users()->attach(JWTAuth::parseToken()->authenticate()->id);
  }

  public function afterUpdate ($oldValue, $request, $id) {
    if ($request->contents) {
      $subject = Horary::find($id);
      $subject->contents()->delete();
      $subject->contents()->createMany($request->contents);
    }
  }

  public function horariesUser (Request $request) {
    $result = $this->model->with($this->with)->horaryUser()->get();

    return response()->json($result);
  }
}

<?php

namespace App\Http\Controllers;

use App\Models\HoraryUser;

class HoraryUserController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (HoraryUser $model) {
    $this->model = $model;
    $this->with  = [];
  }
}

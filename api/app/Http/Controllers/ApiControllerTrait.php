<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

trait ApiControllerTrait {

  public function index (Request $request) {
    $order = $request->all()['order'] ?? ' id asc ';
    $where    = $request->all()['where'] ?? true;

    $result = $this->model->whereRaw($where)
      ->orderByRaw($order)
      ->with($this->with ?? [])
      ->get();

    return response()->json($result);
  }

  public function store (Request $request) {
    if (method_exists($this, 'beforeStore'))
      $this->beforeStore($request);

    $result = $this->model->create($request->all());

    if (method_exists($this, 'afterStore'))
      $this->afterStore($result, $request);

    $result  = $this->model->with($this->with ?? [])->find($result->id);
    return response()->json($result);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show ($id) {
    $result = $this->model->with($this->with ?? [])->findOrFail($id);

    return response()->json($result);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int                      $id
   *
   * @return \Illuminate\Http\Response
   */
  public function update (Request $request, $id) {
    $oldValue = $this->model->findOrFail($id);
    if (method_exists($this, 'beforeUpdate'))
      $this->beforeUpdate($oldValue, $request, $id);

    $this->model->findOrFail($id)->update($request->all());

    if (method_exists($this, 'afterUpdate'))
      $this->afterUpdate($oldValue, $request, $id);

    $result = $this->model->with($this->with ?? [])->findOrFail($id);

    return response()->json($result);
  }

  public function updateMany (Request $request) {
    foreach ($request->all() as $item) {
      $objeto = $this->model->findOrFail($item['id']);
      $objeto->update($item);
      $result[] = $objeto;
    }

    return response()->json($result);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy ($id) {
    $result = $this->model->with($this->with ?? [])->findOrFail($id);
    $result->delete();

    return response()->json($result);
  }


//    public function destroyMany(Request $request) {
//        dd($request->all());
//        foreach ($request->all() as $item) {
//            $objeto = $this->model->findOrFail($item['id']);
//            $objeto->delete($item);
//            $result[] = $objeto;
//        }
//
//        return response()->json($result);
//    }
}
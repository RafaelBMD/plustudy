<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller {
  use \App\Http\Controllers\ApiControllerTrait;
  protected $model;
  protected $with;

  public function __construct (User $model) {
    $this->model = $model;
    $this->with  = [];
  }
}

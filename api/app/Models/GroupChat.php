<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class GroupChat extends Model {
  protected $table = 'group_chat';
  protected $fillable = [
    'user_id',
    'group_id',
    'id_value',
  ];

  protected static function boot () {
    parent::boot();

    static::creating(function (Model $model) {
      // é verificado se a user_id existe primeiro, para os casos de precisar vir o id do usuario do front end
      $user_id        = $model->user_id ? $model->user_id : (JWTAuth::getToken() ? JWTAuth::parseToken()->authenticate()->id : 0);
      $model->user_id = $user_id;
    });
  }

  public function group () {
    return $this->belongsTo(Group::class);
  }

  public function user () {
    return $this->belongsTo(User::class);
  }
}

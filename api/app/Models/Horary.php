<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class Horary extends Model {
  protected $table = 'horaries';
  protected $fillable = [
    'id_value',
    'user_id',
    'begin',
    'end',
    'description',
    'group_id',
    'subject_id',
  ];

  protected $appends = ['participate'];

  protected static function boot () {
    parent::boot();
    // Insere quem criou o horario
    static::creating(function (Model $model) {
      $user_id        = $model->user_id ? $model->user_id : (JWTAuth::getToken() ? JWTAuth::parseToken()->authenticate()->id : 0);
      $model->user_id = $user_id;
    });
  }

  public function contents () {
    return $this->belongsToMany('App\Models\Content');
  }

  public function users () {
    return $this->belongsToMany('App\User');
  }

  public function group () {
    return $this->belongsTo('App\Models\Group');
  }

  public function subject () {
    return $this->belongsTo('App\Models\Subject');
  }

  public function scopeHoraryUser ($query) {
    $query->whereHas('users', function ($query) {
      $query->where('users.id', '=', JWTAuth::parseToken()->authenticate()->id);
    });
  }

  public function getParticipateAttribute () {
    //Retorna true se o usuario paricipa do horary ou false caso não participa
    $result = HoraryUser::where('user_id', '=', JWTAuth::parseToken()->authenticate()->id)
      ->where('horary_id', '=', $this->attributes['id'])->select('id')->first();

    return $result ? $result->id : false;
  }
}

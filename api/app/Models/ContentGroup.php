<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentGroup extends Model {
  protected $table = 'content_group';
  protected $fillable = [
    'content_id',
    'group_id',
  ];
}

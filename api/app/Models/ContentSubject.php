<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentSubject extends Model {
  protected $table = 'content_subject';
  protected $fillable = [
    'content_id',
    'subject_id',
  ];
}

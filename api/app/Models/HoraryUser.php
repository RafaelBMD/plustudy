<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\UserGlobalScope;

class HoraryUser extends Model {
  use UserGlobalScope;

  protected $table = 'horary_user';
  protected $fillable = [
    'horary_id',
    'user_id',
  ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\UserGlobalScope;

class Subject extends Model {
  use UserGlobalScope;

  protected $table = 'subjects';
  protected $fillable = [
    'id_value',
    'user_id',
    'color',
  ];

  public function contents () {
    return $this->belongsToMany('App\Models\Content');
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentHorary extends Model {
  protected $table = 'content_horary';
  protected $fillable = [
    'content_id',
    'horary_id',
  ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class Group extends Model {

  protected $table = 'groups';
  protected $fillable = [
    'id_value',
    'user_id',
    'themes',
  ];

  protected static function boot () {
    parent::boot();

    static::creating(function (Model $model) {
      // é verificado se a user_id existe primeiro, para os casos de precisar vir o id do usuario do front end
      $user_id        = $model->user_id ? $model->user_id : (JWTAuth::getToken() ? JWTAuth::parseToken()->authenticate()->id : 0);
      $model->user_id = $user_id;
    });
  }

  public function contents () {
    return $this->belongsToMany('App\Models\Content');
  }

  public function horaries () {
    return $this->hasMany('App\Models\Horary');
  }

  public function users () {
    return $this->belongsToMany('App\User');
  }

  public function scopeIsParticipant ($query) {
    $query->whereHas('users', function ($query) {
      $query->where('users.id', '=', JWTAuth::parseToken()->authenticate()->id);
    });
  }


}

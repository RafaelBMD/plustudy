<?php
namespace App\Scopes;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

trait UserGlobalScope {
  protected static function boot () {
    parent::boot();

    static::addGlobalScope(new UserScope());

    static::creating(function (Model $model) {
      // é verificado se a user_id existe primeiro, para os casos de precisar vir o id do usuario do front end
      $user_id        = $model->user_id ? $model->user_id : (JWTAuth::getToken() ? JWTAuth::parseToken()->authenticate()->id : 0);
      $model->user_id = $user_id;
    });
  }
}

<?php
/**
 * Created by PhpStorm.
 * User: RafaelBMD
 * Date: 04/04/2018
 * Time: 17:20
 */

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserScope implements Scope {
  public function apply (Builder $builder, Model $model) {
    $user_id = JWTAuth::getToken() ? JWTAuth::parseToken()->authenticate()->id : $model->user_id;
    $builder->where('user_id', $user_id ?? 1);
  }
}

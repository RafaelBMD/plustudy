<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentHoraryTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {
    Schema::create('content_horary', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('content_id')->unsigned();
      $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
      $table->integer('horary_id')->unsigned();
      $table->foreign('horary_id')->references('id')->on('horaries')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {
    Schema::dropIfExists('content_horary');
  }
}

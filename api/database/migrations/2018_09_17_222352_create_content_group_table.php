<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentGroupTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   * tabela usada para permitir o usuário salvar contudos direto da tela de conteudos.
   */
  public function up () {
    Schema::create('content_group', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('group_id')->unsigned();
      $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
      $table->integer('content_id')->unsigned();
      $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {
    Schema::dropIfExists('content_group');
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Alter0Tables extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {
    Schema::table('contents', function (Blueprint $table) {
      $table->boolean('done')->default(false)->change();
      $table->text('description')->nullable()->change();
    });
    Schema::table('groups', function (Blueprint $table) {
      $table->string('themes')->nullable()->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
}

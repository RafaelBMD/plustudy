<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {
    Schema::create('horaries', function (Blueprint $table) {
      $table->increments('id');
      // Quem criou o horario
      $table->integer('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->integer('subject_id')->unsigned()->nullable();
      $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
      $table->integer('group_id')->unsigned()->nullable();
      $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');

      $table->dateTime('begin');
      $table->dateTime('end');
      $table->string('id_value');
      $table->text('description')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {
    Schema::dropIfExists('horaries');
  }
}

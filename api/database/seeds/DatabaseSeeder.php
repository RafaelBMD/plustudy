<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run () {
    // factory(\App\Models\Subject::class, 5)->create();
    factory(App\User::class)->create([
      'name'  => 'Rafael Domingos',
      'email' => 'rafaelbmd3@gmail.com',
    ]);

    factory(App\User::class)->create([
      'name'  => 'Renato Sousa',
      'email' => 'renato@gmail.com',
    ]);

    // $this->call(UsersTableSeeder::class);
  }
}

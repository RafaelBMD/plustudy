
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      {
        path: '',
        component: () => import('pages/index')
      },
      {
        path: '/subject',
        name: 'Subject',
        component: () => import('pages/subject')
      },
      {
        path: '/calendar',
        name: 'Calendar',
        component: () => import('pages/calendar')
      },
      {
        path: '/calendar2',
        name: 'Calendar2',
        component: () => import('pages/calendar2')
      },
      {
        path: '/calendar3',
        name: 'Calendar3',
        component: () => import('pages/calendar3')
      }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]

const view = {index: 1, create: 2, edit: 3}
export default {

  data: () => ({
    dialog: false,
    view: view.index,
    search: '',
    resBody: {},
    allItems: [],
    items: [],
    editedIndex: -1,
    editedItem: {},
    defaultItem: {}
  }),
  methods: {
    reloadLocalStorage(items = this.items) {
      localStorage.setItem(this.entity, JSON.stringify(items));
    },
    insertOfflineItem(table, type, item = this.editedItem) {
      Object.assign(item, {'table': table, 'type': type})
      let updateItems = JSON.parse(localStorage.getItem('updateItems') || '[]');
      // let id_off = (parseInt(localStorage.getItem('ids') || 0)) + 1;
      // item['id_off'] = id_off
      updateItems.push(item)
      localStorage.setItem('updateItems', JSON.stringify(updateItems));
      // localStorage.setItem('ids', id_off);
    },
    initialize() {
      if (navigator.onLine) {
        this.$axios.get(`${this.entity}`).then((res) => {
          this.items = res.data
        }).catch(() => {
          this.$q.notify({
            color: 'negative',
            position: 'top',
            message: 'Falha no carregamento',
            icon: 'report_problem'
          })
        })
      } else {
        this.items = JSON.parse(localStorage.getItem(this.entity))
      }
    },
    create() {
      if (this.beforeCreate()) {
        // this.errors.clear()
        this.dialog = true
        this.view = view.create
        this.editedIndex = 0
        this.editedItem = JSON.parse(JSON.stringify(this.defaultItem))
        this.afterCreate()
      }
    },
    editItem(item) {
      if (this.beforeEdit(item)) {
        // this.errors.clear()
        this.dialog = true
        this.view = view.edit
        this.editedIndex = this.items.indexOf(item)
        this.editedItem = Object.assign(this.editedItem,
          JSON.parse(JSON.stringify(item)))
        this.afterEdit(item)
      }
    },
    deleteItem(item, insertOff = true) {
      const index = this.items.indexOf(item)
      if (confirm('Tem certeza que deseja excluir esse item?')) {
        console.log(navigator.onLine)
        if (navigator.onLine) {
          if (this.beforeDelete(item)) {
            this.$axios.delete(`${this.entity}/${item.id}`).then(
              () => {
                this.items.splice(index, 1)
                this.reloadLocalStorage()
                this.afterDelete(item)
              },
              err => function () {
                console.log(err)
              }
            )
          }
        } else {
          if (this.beforeDelete(item)) {
            this.items.splice(index, 1)
            if (insertOff) {
              this.reloadLocalStorage()
              this.insertOfflineItem(this.entity, 'delete', {id: item.id})
            }
            this.afterDelete(item)
          }
        }
      }
    },
    close(time = 0) {
      if (this.beforeClose()) {
        this.dialog = false
        setTimeout(() => {
          this.view = view.index
          Object.assign(this.editedItem,
            JSON.parse(JSON.stringify(this.defaultItem)))
          this.editedIndex = -1
        }, time)
        this.afterClose()
      }
    },
    save(saveNewView = null, notClose = true) {
      // this.$validator.validateAll().then((result) => {
      //   if (!result) {
      //     alert('Por favor, corrija as validações antes de salvar!')
      //     return false
      //   }

      this.resBody = JSON.parse(JSON.stringify(this.editedItem))
      if (this.view === 2) {
        if (this.beforeInsert(this.editedItem)) {
          if (navigator.onLine) {
            this.$axios.post(this.entity, this.editedItem).then((res) => {
              this.resBody = res
              // è verificado o tamanho do objeto, caso o item criado não seja para o usuário, exemplo: criar um grupo sem que
              // o usuário se coloque como participante, não aparecendo na lista apos a criação
              if (Object.keys(res.data).length > 0) {
                this.items.push(res.data)
                this.reloadLocalStorage()
              }
              this.afterInsert(res.data)
              this.close(0, saveNewView)
            }).catch(() => {
              this.$q.notify({
                color: 'negative',
                position: 'top',
                message: 'Loading failed',
                icon: 'report_problem'
              })
            })
          } else {
            if (Object.keys(this.resBody).length > 0) {
              // busca o id  do contador que esta no localstorage
              let id = (parseInt(localStorage.getItem('ids') || 1000000)) + 1;
              this.resBody['id'] = id
              this.items.push(this.resBody)
              localStorage.setItem('ids', id);

              this.reloadLocalStorage()
              this.insertOfflineItem(this.entity, 'create', this.resBody)
            }
            this.afterInsert(this.resBody)
            this.close(0, saveNewView)
          }
        } else {
          if (!notClose) {
            this.close(0, saveNewView)
          }
        }
      } else if (this.view === 3
      ) {
        let index = this.editedIndex
        if (this.beforeUpdate(this.editedItem)) {
          if (navigator.onLine) {
            this.$axios.put('' + this.entity + '/' + this.editedItem.id,
              this.editedItem).then(
              (res) => {
                this.resBody = res.data
                Object.assign(this.items[index], res.data)
                this.reloadLocalStorage()
                this.afterUpdate(res.data)
                this.close(0, saveNewView)
              }
            ).catch(() => {
              this.$q.notify({
                color: 'negative',
                position: 'top',
                message: 'Loading failed',
                icon: 'report_problem'
              })
            })
          } else {
            Object.assign(this.items[index], this.resBody)
            this.reloadLocalStorage()
            this.insertOfflineItem(this.entity, 'update', this.resBody)
            this.afterUpdate(this.resBody)
            this.close(0, saveNewView)
          }
        } else {
          this.close(0, saveNewView)
        }
      }
// })
    },
    saveNew() {
      this.save(view.create)
      setTimeout(() => {
        this.create()
      }, 0)
    }
    ,
    saveEdit() {
      this.save()
      setTimeout(() => {
        this.editItem(this.resBody)
      }, 1000)
    }
    ,
    beforeCreate() {
      return true
    }
    ,
    afterCreate() {
    }
    ,
    beforeEdit(oldValues) {
      return true
    }
    ,
    afterEdit(oldValues) {
    }
    ,
    beforeInsert(oldValues) {
      return true
    }
    ,
    afterInsert(newValues) {
    }
    ,
    beforeUpdate(oldValues) {
      return true
    }
    ,
    afterUpdate(newValues) {
    }
    ,
    beforeDelete(oldValues) {
      return true
    }
    ,
    afterDelete(oldValues) {
    }
    ,
    beforeClose() {
      return true
    }
    ,
    afterClose() {
    }
    ,
    getMaxValue(arr, prop) {
      let max
      max = 0
      for (let i = 0; i < arr.length; i++) {
        if (parseInt(arr[i][prop]) > parseInt(max)) {
          max = arr[i][prop]
        }
      }
      return (max || 0)
    }
  },
  computed: {
    formTitle() {
      switch (this.view) {
        case 1:
          return this.translate('system.title.consult') +
            this.translate('table.' + this.entity + '.title')[1]
        case 2:
          return this.translate('system.title.include') +
            this.translate('table.' + this.entity + '.title')[0]
        case 3:
          return this.translate('system.title.edit') +
            this.translate('table.' + this.entity + '.title')[0]
      }
    }
  }
}

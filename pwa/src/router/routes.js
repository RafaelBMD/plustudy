export default [
  // {
  // path: '',
  // component: () => import('layouts/Out'),
  // children: [
  {
    path: '/login',
    name: 'Login',
    component: () => import('pages/auth/Login')
    // },
    // {
    //       path: '/register',
    //       name: 'Register',
    //       component: () => import('pages/auth/Register')
    //     }, {
    //       path: '/forgot',
    //       name: 'Forgot',
    //       component: () => import('pages/auth/Forgot')
    //     }, {
    //       path: '/reset/:token',
    //       name: 'Reset',
    //       component: () => import('pages/auth/Reset')
    //     }]
  }, {
    path: '',
    component: () => import('layouts/In'),
    meta: { requiresAuth: true },
    children: [
      {path: '', component: () => import('pages/calendar')},
      {
        path: '/calendar',
        name: 'Calendar',
        component: () => import('pages/calendar')
      }, {
        path: '/subject',
        name: 'Subject',
        component: () => import('pages/subject')
      }, {
        path: '/horary',
        name: 'Horary',
        component: () => import('pages/horary')
      }, {
        path: '/group',
        name: 'Group',
        component: () => import('pages/group')
      }]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]

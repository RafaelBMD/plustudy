// Configuration for your app

module.exports = function (ctx) {
  return {
    // app plugins (/src/plugins)
    plugins: [
      'axios',
      'auth-router'
    ],
    css: [
      'app.styl'
    ],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      'material-icons' // optional, you are not bound to it
      // 'ionicons',
      // 'mdi',
      // 'fontawesome'
    ],
    supportIE: false,
    vendor: {
      add: [],
      remove: []
    },
    build: {
      scopeHoisting: true,
      // proxy: {
      // "/api": {
      //   target: "http://app.plustudy.com.br",
      //   pathRewrite: {"^/backend/api" : "api"}
      // }
      // },
      // vueRouterMode: 'history',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      // proxy: {
      //   "/backend/api": {
      //     target: "http://api.plustudy.com.br",
      //     pathRewrite: {"^/backend/api" : "api"}
      //   }
      // },
      extendWebpack (cfg) {
      }
    },
    devServer: {
      // https: true,
      // proxy: {
      //   '/api': {
      //     target: 'https://api.plustudy.com.br',
          // target: 'http://127.0.0.1:8000',
          // pathRewrite: {'^/api': 'api'}
        // }
      // }
      // https: true,
      // port: 8080,
      open: true // opens browser window automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      components: [
        'QLayout',
        'QLayoutHeader',
        'QLayoutDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QIcon',
        'QList',
        'QListHeader',
        'QItemSeparator',
        'QItemTile',
        'QItem',
        'QItemMain',
        'QItemSide',
        'Dialog',
        'QDialog',
        'QField',
        'QInput',
        'QDatetime',
        'QDatetimePicker',
        'QCheckbox',
        'QUploader',
        'QModal',
        'QModalLayout',
        'QFab',
        'QFabAction',
        'QSelect',
        'QPopover',
        'QChipsInput',
        'QChip',
        'QTabs',
        'QTab',
        'QTabPane',
        'QRouteTab',
        'QChatMessage',
        'QCard',
        'QCardSeparator',
        'QCardActions',
        'QAjaxBar',
        'QTooltip',
      ],
      directives: [
        'CloseOverlay',
        'Ripple'
      ],
      // Quasar plugins
      plugins: [
        'Notify'
      ],
      // iconSet: ctx.theme.mat ? 'material-icons' : 'ionicons'
      i18n: 'pt-br' // Quasar language
    },
    // animations: 'all' --- includes all animations
    animations: [],
    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {},
      manifest: {
        name: 'Plustudy',
        short_name: 'Plustudy',
        description: 'Gerenciador de estudos!',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      // bundler: 'builder', // or 'packager'
      extendWebpack (cfg) {
        // do something with Electron process Webpack cfg
      },
      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      },
      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'quasar-app'
      }
    }
  }
}
